import torch
from torch.utils.data import DataLoader, Dataset
from torch.autograd import Variable

import cv2
import numpy as np
from pathlib import Path
import tools
from tools import *
from tools.transforms import *
from tools.saliency import *

class Resize(object):
    def __init__(self, new_dims):
        if isinstance(new_dims, int): self.dims = (new_dims, new_dims)
        else: self.dims = new_dims
    def resize_image(self, im):
        return cv2.resize(im, (self.dims))
    def __call__(self, im):
        return self.resize_image(im)

class OvenDataset(Dataset):
    def __init__(self, data_dir, mode):
        super().__init__()
        """
        'i', 'l' in variable names stand for input and label respectively
        """
        
        data_dir = Path(data_dir + 'oven')
        input_format, label_format = '.jpg', ''

        raw_i_paths = [j for i in data_dir.glob('*') if i.is_dir() for j in i.glob('**/*' + input_format)]
        
        # prepare the dataset
        i_paths, i_labels = [], []
        for i_path in raw_i_paths:
            class_name = i_path.parts[-2]
            i_paths.append(i_path)
            i_labels.append(class_name)
        
        classes = list(np.unique(i_labels))
        class_freq = [i_labels.count(i) for i in classes]
        
        n_inputs, n_labels, n_classes = len(np.unique(i_paths)), len(i_labels), len(classes)
        assert n_inputs == n_labels
        
        print('Num. of images: %d | Num. of classes: %d | Min/Max number of images per class: %d/%d' \
              %(len(i_paths), n_classes, np.min(class_freq), np.max(class_freq)))
        
        # one_hotting the labels - function     
        classes_oh = torch.eye(len(classes)).numpy()
        i_labels_idx = [classes.index(i) for i in i_labels]
        i_labels_oh = [classes_oh[i] for i in i_labels_idx]
        
        self.n_classes = n_classes
#         print(list(classes_oh), classes)
#         self.class_mapping = {i:j for i,j in zip(list(classes_oh), classes)}
#         print(self.class_mapping)
        self.data_paths = {'input_paths': i_paths, 'input_labels': i_labels_oh}
        
        # shuffle and split data
        np.random.seed(0)
        ss_ix = np.random.permutation(n_inputs)
        ss_ixs = {'train': ss_ix[:len(ss_ix) * 4 // 5], 
#                   'eval': ss_ix[len(ss_ix) * 3 // 5: len(ss_ix) * 4 // 5],
                  'eval': ss_ix[len(ss_ix) * 4 // 5:]}
        
        self.dataset = {i: np.array(j)[ss_ixs[mode]] for i, j in self.data_paths.items()}
        
        print('Setting "%s" dataset with %d images-label pairs' %(mode, len(np.unique(self.dataset['input_paths']))))
        
        self.dataset_size = len(self.dataset['input_paths'])
        
        self.basic_transforms = [
            RandomCrop(512),
            Resize(32),
#             FlipY,
#             FlipX,
        ]
        
        self.aug_transforms = [
            RandomRotation(),
            
        ]
        
        self.sal_transforms = [
            PHOT,  
            strukturtrnsor,
            AC,
            Darker,
            BMS,
            Mcue2
        ]
    
    def __getitem__(self, index):
        
        input_path, input_label = self.dataset['input_paths'][index], self.dataset['input_labels'][index]
        input_im, input_lb = read_image(input_path), input_label
        
        for t in self.basic_transforms:                                  
            input_im = t(input_im)

        for t in self.aug_transforms:
            input_gim = t(input_im) 
        
#         input_sal = None
#         saliency = False
#         if saliency:
#             for t in self.sal_transforms:
#                 input_sal = t(input_im) 
        
        input_im_tensor = Variable(torch.Tensor(to_tensor_shape(input_im)))
        input_lb_tensor = Variable(torch.LongTensor(input_lb))
        
        return input_im_tensor, input_lb_tensor
    
    def __len__(self):
        return self.dataset_size
    
    def __nclasses__(self):
        return self.n_classes
    
# for _, data in enumerate(dataloaders['train']):
#     ims, labels = data
# #     print(ims.shape, labels)
# #     plot_ims(ims)
#     plt.imshow(ims[0].data.numpy().astype('int')[:,:,[2,1,0]])