import numpy as np
import torch
import torch.nn as nn

def test_model(device, dataloaders, model, loss_fn=None, epochs=1, verbose=True, max_bs=32):
    loss_code = "CL" 
    modes = ['eval']

    epoch_log = {i: [] for i in range(epochs)} # {epoch: (im, pred, label)}
    preds, labels = [], []
    
    print('\nTesting...')
    for e in range(epochs):
        for mode in modes:
            per_batch_losses = []
            for _, batch in enumerate(dataloaders[mode]): 
                
                ims, labels = batch
                
                ims = ims.to(device)
                preds = model(ims)

                if isinstance (loss_fn, torch.nn.CrossEntropyLoss): 
                    labels = labels.max(dim=1)[1]
                loss = loss_fn(preds, labels)
                # log                 
                per_batch_losses.append(loss.item())
                
#                 print(loss, loss.data, loss.item())
#                 epoch_log[e]['ims'], epoch_log[e]['preds'], epoch_log[e]['labels'] = ims, preds, labels
#                 epoch_log[e]['loss'] = loss
                epoch_log[e].append((ims, preds, labels))
                    
            epoch_loss = np.asarray(per_batch_losses).mean()
#             epoch_loss = torch.mean(per_batch_losses)
    
            if verbose:
                print('epoch: %d | %s loss: %.10f' %(e, loss_code, epoch_loss))
                                
    return epoch_log, per_batch_losses
