import torch
import torch.nn as nn
from torch.utils.data import DataLoader
import sys
from importlib import reload

print('Python | Pytorch versions: %s | %s' %(sys.version, torch.__version__))

import tools
reload(tools)
from tools.utils import *
from tools.visuals import *
from datasets import OvenDataset
import model
from model.model import * 
from train_eval import *
from test import *

if __name__ == "__main__":

    use_cuda = torch.cuda.is_available() 
    # sand not args.no_cuda
    device = torch.device('cuda' if use_cuda else 'cpu')
    torch.manual_seed(0)
    if use_cuda: torch.cuda.manual_seed(0)

    conf_file = {
        'input_nc': 3, 
        'output_nc': 3,
        'ngf': 64,                # of gen filters in first conv layer'
        'n_downsample_global': 1, # number of downsampling layers in netG
        'n_blocks_global': 1,     # number of residual blocks in the global generator network
        'norm': 'switch',
        'gpu_ids': [],
        'padding_type': 'replicate',
        'epochs': 10,
        'data_dir': '../data/',
        'modes': ['train', 'eval']
    }

    # setting an encoder as classifier
    for k, v in conf_file.items():
        exec ("%s=v" %k)

    datasets = {mode: OvenDataset(data_dir, mode) for mode in modes}
    dataloaders = {mode: DataLoader(datasets[mode], batch_size=8, shuffle=True, num_workers=0, drop_last=False) 
                           for mode in modes} 
    n_classes = datasets['train'].n_classes
    model = define_net(conf_file, input_nc, output_nc, ngf, model='encoder', norm='batch', n_classes=n_classes,
                               n_downsample_global=n_downsample_global, padding_type=padding_type)

    optimizer = torch.optim.Adam(model.parameters(), lr=1e-2)
    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=50)
    loss = nn.CrossEntropyLoss()
    trn_log, emb_log, losses = train_and_eval_model(device, dataloaders, model, optimizer, 
                                                    loss_fn=loss, scheduler=scheduler, epochs=1, verbose=True)
    tst_log, pbl = test_model(device, dataloaders, model, loss_fn=loss, verbose=True)
    plot_results(tst_log, n_classes)
